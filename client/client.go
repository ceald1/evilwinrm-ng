package client

import (
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)


const(
soapenv = "http://www.w3.org/2003/05/soap-envelope"
soapaddr = "http://schemas.xmlsoap.org/ws/2004/08/addressing"
wsmanfault = "http://schemas.microsoft.com/wbem/wsman/1/wsmanfault"
wmierror = "http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/MSFT_WmiError"
)

var port = 5985


func RunClient (username string, password string, host string) {
	url := fmt.Sprintf("http://%s:%d/wsman", host, port)
	auth := base64.StdEncoding.EncodeToString([]byte(username + ":" + password))


}