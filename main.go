package main


import (
    // "context"
    "fmt"
    "os"
	"ceald/winrm/client"
    "github.com/urfave/cli/v2"
)

func main() {
    var host string
    var password string
    ssl := false
    var user_hash string
    port := 5985
    var domain string
    var username string
    KrbConf := "/etc/krb5.conf"
    var spn string

    winrm_args := &cli.App{
        Name:  "Winrm-ng",
        Usage: "A go version of 'evil-winrm'",
        Flags: []cli.Flag{
            &cli.StringFlag{
                Name:        "host",
                Aliases:     []string{"i"},
                Usage:       "Host flag, can be either IP or hostname",
                Destination: &host,
            },
            &cli.StringFlag{
                Name:        "username",
                Aliases:     []string{"u"},
                Usage:       "username",
                Destination: &username,
            },
            &cli.StringFlag{
                Name:        "password",
                Aliases:     []string{"p"},
                Usage:       "Password flag is for password authentication",
                Destination: &password,
            },
            &cli.StringFlag{
                Name:        "hash",
                Aliases:     []string{"H"},
                Usage:       "hash for user",
                Destination: &user_hash,
            },
            &cli.BoolFlag{
                Name:        "ssl",
                Usage:       "ssl for winrm?",
                Destination: &ssl,
            },
            &cli.StringFlag{
                Name:        "domain",
                Aliases:     []string{"d"},
                Usage:       "domain for user",
                Destination: &domain,
            },
            &cli.StringFlag{
                Name:        "kerb_config",
                Aliases:     []string{"kc"},
                Usage:       "location for kerberos config, default is: `/etc/krb5.conf`",
                Destination: &KrbConf,
            },
            &cli.StringFlag{
                Name:        "spn",
                Usage:       "spn for user (used with kerberos auth)",
                Destination: &spn,
            },
            &cli.IntFlag{
                Name:        "port",
                Usage:       "port for winrm, default is: 5985",
                Destination: &port,
                Value:       5985,
            },
        },
        Action: func(ctx *cli.Context) error {
            fmt.Println("Hello world 😀")
			client.RunClient()

            return nil
        },
    }
    winrm_args.Run(os.Args)
}